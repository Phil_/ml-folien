# WGAN

<!---
## Grundansatz des Lernens einer Verteilung
- Parametrisches Modell
- Log-Likelihood
- $\underset{\theta \in \mathbb{R}^d}{\max}\frac{1}{m}\sum_{i=1}^m\log(p_\theta(x_i))$

## Kullback-Leibler Divergenz
- $KL(\mathbb{P}_X||\mathbb{P}_\theta)=\int p_X \log\left(\frac{p_X}{p_\theta}\right)$
- Benutzt beim klassischen Gan
- Maximierung von log Likelihood ist asympotitsch Kullback-Leibler
- Maß für Ähnlichkeit zweier Verteiungen


## Schwächen von Kullback-Leibler
- Zufallsgrößen müssen dichte besitzen
- $KLhttps://i.imgur.com/0iOj6Xf.png(\mathbb{P}_X||\mathbb{P}_\theta) = -\infty$ bei $p_X\neq0 \land p_\theta = 0$ 
- Rauschen im Modell für konvergenz benötigt
- Rauschen macht Bilder unscharf
-->

<!-- ## Was heißt Konvergenz vom Modell? -->
### Statistischer Blickwinkel
Machine Learning | Statistik
:-------------------------:|:-------------------------:
Modell | parametische Verteilung $\mathbb{P}_{\theta}$
Training | Folge von Verteilungen $(\mathbb{P}_{\theta(t)})_{t \in \mathbb{N}}$
Loss | Ähnlichkeitsmaß : $\rho(\mathbb{P}_{\theta},\mathbb{P}_{X})$

- <span style="color:red"> *Konvergenz hängt stark von dem Ähnlichkeitsmaß $\rho$ ab* </span> 
<!--- 
- Konvergenz : $\rho(\mathbb{P}_{\theta(t)}, \mathbb{P}_{X})\underset{t \rightarrow \infty}{\rightarrow} 0$ 
--->
<!---
## Alt
- Modell : parametische Verteilung $\mathbb{P}_{\theta}$
- Training : Folge von Verteilungen $(\mathbb{P}_{\theta(t)})_{t \in \mathbb{N}}$
- Ähnlichkeitsmaß : $\rho(\mathbb{P}_{\theta},\mathbb{P}_{X})$
--->
<!--- rho ist kein maß der maßtheorie-->

### Was macht ein gutes Ähnlichkeitsmaß aus?
- Ziel: **Optimierung**
  - Gradient-Descent $\Rightarrow$ Stetigkeit und Differenzierbarkeit
  
- Modell-Funktion
  - $\theta \rightarrow \mathbb{P}_{\theta}$ ist stetig-differenzierbar weil NN

- Loss-Funktion 
  <!--- - Loss : $\theta \rightarrow \rho(\mathbb{P}_\theta,\mathbb{P}_X)$
  - Ähnlichkeitsmaß soll auch stetig-differenzierbar sein --->
  - $\theta \rightarrow \rho(\mathbb{P}_\theta,\mathbb{P}_X)$ ist nicht immer stetig-differenzierbar 


<!--- - Total Variation (TV) $$\delta(\mathbb{P}_{\theta},\mathbb{P}_{X}) = \underset{A \in \Sigma}{\sup\left|\mathbb{P}_{X}(A)-\mathbb{P}_{\theta}(A) \right|}$$ --->
### Vergleich von Ähnlichkeitsmaßen
#### Starke Ähnlichkeitsmaße
- Kullback-Leibler-Divergenz $$KL(\mathbb{P}_X || \mathbb{P}_{\theta}) = \int_{-\infty}^{\infty} p_{X}(x) \cdot \text{ln} \left( \frac{p_{X}(x)}{p_{\theta}(x)} \right)dx$$
- Jensen-Shannon-Divergenz (JS) $$JS(\mathbb{P}_X||\mathbb{P}_\theta) = KL(\mathbb{P}_X||\mathbb{P}_m) + KL(\mathbb{P}_\theta||\mathbb{P}_m)$$

### Vergleich von Ähnlichkeitsmaßen
#### Schwache Ähnlichkeitsmaße
- Earth-Mover-Distanz (EM) oder Wasserstein-1 $$W(\mathbb{P}_{\theta},\mathbb{P}_{X}) = \underset{\gamma \in \Pi(\mathbb{P}_\theta,\mathbb{P}_X)}{\inf}\underset{{\:\:\:\:(x,y)\sim \gamma}}{\mathbb{E}}\left[||x-y||\right]$$
- $\Pi(\mathbb{P}_\theta,\mathbb{P}_X) =\mathrel{\mathop:}$  Menge der Zufallsgrößen mit $\mathbb{P}_\theta,\mathbb{P}_X$ als Randverteilungen.

### Vergleich von Ähnlichkeitsmaßen
#### Wie lässt sich EM verstehen?
$$W(\mathbb{P}_{\theta},\mathbb{P}_{X}) = \underset{\gamma \in \Pi(\mathbb{P}_\theta,\mathbb{P}_X)}{\inf}\underset{{\:\:\:\:(x,y)\sim \gamma}}{\mathbb{E}}\left[||x-y||\right]$$

- Verschieben des Volumens unter den Dichten

![](graphics/em_schaubild.png){width=65%}
<small> Quelle: [https://towardsdatascience.com/earth-movers-distance-68fff0363ef2](https://towardsdatascience.com/earth-movers-distance-68fff0363ef2) </small>

<!--- Gutes Schaubild machen/finden die Simple erklärt wie die eine verteilung zur anderen verteilung gemacht bringt --->

### Vergleich von Ähnlichkeitsmaßen
#### Ein kleines Beispiel
- $Z \sim \text{UNI}(0,1)$
- Target: $(0,Z) \in \mathbb{R}^2 \sim \mathbb{P}_0$
- Modell: $g_\theta(z) = (\theta,z) \in \mathbb{R}^2 \sim \mathbb{P}_\theta$


![](graphics/low_dim_manifold.png){width=60%}

### Vergleich von Ähnlichkeitsmaßen
#### Ein kleines Beispiel
- $W(\mathbb{P}_{\theta},\mathbb{P}_{X})=|\theta|$
- $JS(\mathbb{P}_{\theta},\mathbb{P}_{X})=\begin{cases} \ln(2) &\mbox{if } \theta \neq 0 \\
0 & \mbox{if } \theta = 0 \end{cases}$
- $KL(\mathbb{P}_{\theta},\mathbb{P}_{X})=KL(\mathbb{P}_{\theta},\mathbb{P}_{X})=\begin{cases} \infty &\mbox{if } \theta \neq 0 \\
0 & \mbox{if } \theta = 0 \end{cases}$

![](graphics/em_vs_js.png){width=60%}

### Realisierung von Wasserstein
- $W(\mathbb{P}_{\theta},\mathbb{P}_{X}) = \underset{\gamma \in \Pi(\mathbb{P}_\theta,\mathbb{P}_X)}{\inf} \underset{{\:\:\:\:(x,y)\sim \gamma}}{\mathbb{E}}\left[||x-y||\right]$ 
- Meist nicht lösbar

<!---
### Realisierung von Wasserstein
--->
$\Rightarrow$ **Kantrovich-Rubinstein-Dualität:**

$$W(\mathbb{P}_{\theta},\mathbb{P}_{X}) = \underset{||f||_L \leq 1}{\sup} \underset{{\:x\sim\mathbb{P}_\theta}}{\mathbb{E}}[f(x)] - \underset{{\:\:x\sim\mathbb{P}_X}}{\mathbb{E}}[f(x)]$$

- $||f||_L \leq 1$ : 1-Lipschitzstetige Funktionen

### Realisierung von Wasserstein
#### Optimierungsproblem mit einer parametrisierten Funktion 
- EM $=\underset{w \in \mathcal{W}}{\text{argmax }} \underset{{\:\:x\sim\mathbb{P}_X}}{\mathbb{E}}[f_w(x)] - \underset{\:{z\sim p_z}}{\mathbb{E}}[f_w(g_\theta(z))]$
- $f_w$ : Diskriminator mit Gewichten aus einem kompakten Raum $\mathcal{W}$
- $g_\theta$ : Generator
- <span style="color:red"> *Achtung!* </span> $f_w$ wird nicht als Wahrscheinlichkeit sondern als Score interpretiert 

### WGAN-Trainingsalgorithmus
![](graphics/wgan.png){width=85%}


### Wasserstein-Loss
- Loss geht runter $\Rightarrow$ Samples werden besser

![](graphics/wgans_metric_1.png){width=75%}
<small> Quelle: [https://arxiv.org/abs/1701.07875](https://arxiv.org/abs/1701.07875) </small>



### Jenson-Shannon-Loss
- JS stagniert oder steigt $\Rightarrow$ Samples werden besser

![](graphics/wgans_metric_2.png){width=75%}
<small> Quelle: [https://arxiv.org/abs/1701.07875](https://arxiv.org/abs/1701.07875) </small>


<!---
## (Optional) Gradient Penalty
- Alternative zum Clipping
- Clipping hängt stark vom Clip-Parameter ab
- Vanishing-Gradient oder Instabilität möglich
- Hier kommt nachher die Formel für Gradient-Penalty hin

## Kurze Zusammenfassung
--->
