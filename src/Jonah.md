# Schwierigkeiten beim Training eines GANs

## Schwierigkeiten
- Mode Collapse
- Vanishing Gradients
- Stabilität: Gradient wird zu groß


## Mode Collapse
![](graphics/ModeCollapse.png)
<small> Quelle: [Multi-Agent Diverse Generative Adversarial Networks (2017)] </small>

## Mode Collapse
![](graphics/GAN_Mode_Collappse.png)
<small> Quelle: [Training Generative Adversarial Nets](https://openreview.net/pdf?id=rkmu5b0a-) </small>

## Diskriminator
- Diskriminator maximiert:
    $$\underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x)) + \underset{z}{\mathbb{E}} \text{ ln} (1 - D(G(z)))$$

- KL und JS für Verteilungen $\mathbb{P}_X$ und $\mathbb{P}_{\theta^{(G)}}$ maximal, falls diese disjunkt sind

## Folgen für den Generator

Fehler des Diskriminators           |  Gradient des Generators
:-------------------------:|:-------------------------:
![](graphics\discriminator_acc.PNG){width=100%}| ![](graphics\Generator_gradient.PNG){width=90%}

<small>Quelle: [Towards Principled Methods For Training
Generative Adversarial Networks (2017)](https://arxiv.org/abs/1701.04862)</small>

# Mögliche Lösungen

## Verbesserung Generatorkosten

- Gradient des Generators ersetzen
 
 \begin{align*} \nabla_{\theta}& \underset{z}{\mathbb{E}} \text{ ln} (1 - D(g_{\theta}(z))) \\
 \rightarrow \nabla_{\theta}& \underset{z}{\mathbb{E}} \text{ ln} (D(g_{\theta}(z)))
 \end{align*}

## Verbesserung Generatorkosten

- Generatorkosten vorher
$$\nabla_{\theta}\:2 JS(\mathbb{P}_{\theta^{(G)}}||\mathbb{P}_X)$$
- Generator jetzt
$$
\nabla_{\theta}\left[KL(\mathbb{P}_{\theta^{(G)}}||\mathbb{P}_X)-2JS(\mathbb{P}_{\theta^{(G)}}||\mathbb{P}_X)\right]
$$
- Widerspruch: JS wird subtrahiert
- Idee: KL wird sehr groß bei schlechten Bildern

## Hat's  geholfen?
- Nein
 ![Fixer Generator, Gradient wird groß mit hoher Varianz](graphics\unstable_Gan.PNG)
 <small>Quelle: [Towards Principled Methods For Training
Generative Adversarial Networks (2017)](https://arxiv.org/abs/1701.04862)</small>



## Feature Matching
![](graphics\feature_matching_discri.PNG)
<small>Quelle: [towardsdatascience.com 2018](https://towardsdatascience.com/gan-ways-to-improve-gan-performance-acf37f9f59b)</small>

- Nash Equilibrium schneller finden
- Generator erhält zusätzliche Ziele:
    - Reale Daten imitieren
    - Generator nicht overfitten



## Neues Minimerungsziel: 

- Anpassen des Diskriminatorgradienten
    - $\underset{x \sim p_{X}}{\mathbb{E}}f(x_i)- \underset{z}{\mathbb{E}}f(G(z))$
- $f(x_i)$ ist ein Layer des Diskriminators "Zwischenschritt"
- Generator nicht übertrainieren $f(x_i)$ 
- Verteilung der realer Daten $\underset{x \sim p_{X}}{\mathbb{E}}$

## Minibatch Discrimination
<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

<div class="container">

<div class="col">
![](graphics\feature_matching.PNG)
</div>
<div class="col">
- Tensor $T \in \mathbb{R}^{A\times B\times C}$
- $T \cdot f(x_i)$ erzeugt $A$ Matrizen aus $\mathbb{R}^{B\times C}$
- Abstand der Zeilen: $exp(-\left\lVert M_{i,b} - M_{j,b}\right\rVert_{L_1})$
- Summe der Distanzen als zusätzliches Layer einbinden
</div>
</div>

<small> Quelle: [Improved Techniques for Training GANs (2016)](https://arxiv.org/abs/2006.09011) </small>


## One-sided Label Smoothing
- Regularisierung
- Statt 0 und 1 $\alpha$ bzw. $\beta$ aus $]0,1[$
- Warum ?
    - Generatoren nutzen schwache Diskriminatoren aus
    - Parameter sollen Generator "verunsichern" 
- Folge
    - Schnellere Konvergenz
    - Produktivere Änderungen des Generators

## Historical Averaging
- Die letzten t Parametersätze sammeln
- Strafterm: $\left\lVert \theta - \frac{1}{t} \sum_{i=1}^t \theta_i\right\rVert$
- Jeder Spieler erhält Strafterm
- Alternierende Änderungen werden vermieden

