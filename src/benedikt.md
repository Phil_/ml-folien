# Conditional Generation

<!---
TODO: Mathematische Modell an Philipps und Josefs Vortrag anpassen.
--->
### cGAN-Motivation

* Bisher: Generierung anhand Verteilung der Daten
<!---  * Bsp.: Bilder, die Datensatz ähneln (Gesichter) --->
* Häufig erwünscht: Generierung für Verteilung der Daten unter Bedingung
<!---  * Bsp.: Bilder, die Datensatz ähneln *und* bestimmte Eigenschaften $c$ aufweisen (Gesichter mit Brille) --->
* Lösung: conditional GANs (cGANs)

![](./graphics/conditional_G_results.png)
<small> Quelle: [Conditional Generative Adversarial Nets (2014)](https://arxiv.org/abs/1411.1784) </small>

### Generatoren für cGANs

<!---
TODO: Alternativen zur one-hot-Kodierung
--->

* Zusätzlicher Input-Layer für gewünschte Eigenschaft $y$
  * z.B. one-hot-kodiert
* Input nicht mehr komplett zufällig
* Trainingsdaten müssen gelabelt sein

---

![](./graphics/conditional_G.png){width=100%}
<small> Quelle: [Conditional Generative Adversarial Nets (2014)](https://arxiv.org/abs/1411.1784) </small>

### Diskriminatoren für cGANs

* Vanilla GAN: $D(x)$ bewertet Glaubwürdigkeit des generierten Ergebnisses $x$
* Conditional GAN: $D(x\mid y)$ bewertet Glaubwürdigkeit *und* Übereinstimmung mit gewünschter Eigenschaft $y$

---

![](./graphics/conditional_G_and_D.png)
<small> Quelle: [Conditional Generative Adversarial Nets (2014)](https://arxiv.org/abs/1411.1784) </small>

<!---
* [Alternative: zusätzlicher Klassifikator C (AC-GAN, auxiliary classificator)]
  * [Triple-GAN: Ergebnis von C ist Input von D]
--->

### cGAN-Training

* Bedingte Value-Funktion:

$V(\theta^{(D)}, \theta^{(G)})$
$\ \ = \underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x\mid y_x)) + \underset{z}{\mathbb{E}} \text{ ln} (1 - D(G(z,y)\mid y))$

* Trainingsprinzip des Vanilla-GANs bleibt erhalten
<!---
TODO: MiniMax-Gleichung besprechen
TODO: Quellen einfügen
--->

### cGAN-Beispiel

![](./graphics/Stage1_G_results.png){width=65%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

Vor welches grundlegende Problem stellt uns diese Aufgabenstellung (bezogen auf die benötigten Daten)?

### cGAN-Beispielarchitektur

![](./graphics/Stage1_G_and_D.png){height=100%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

<!---
TODO: Wie wird embedding aus Text erzeugt?
Was bedeutet Conditioning Augmentation?
--->

# StackGAN

### StackGANs-Motivation

<!--- <img src=./graphics/Stage1_G_results.png> --->
![](graphics/Stage1_G_results.png){width=50%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

* Erzeugte Abbildungen sind nur 64x64-Pixel
* Auch GANs mit komplexerem $G$ (mehr Upsampling-Layer) können keine realistischen Bilder mit hoher Auflösung erzeugen
<!---
Außer mit progressive growing -> einfügen?
--->
  <!-- * TODO: Ursachen hierfür -->

---

![](graphics/Stage1_G_results.png){width=50%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

* Lösung: Aufteilung von Generierungs- und Diskriminierungsschritt in mehrere Unteraufgaben
* Zusammenarbeit mehrerer $G_i$ und $D_i$ → StackGAN


<!-- ### Sketch-Refinement-Process

 * Bsp.: Statt direkter Erzeugung eines hochauflösenden Bildes folgende Unteraufgaben
    1. Erzeugung einer niedrigauflösenden Skizze
    2. Erzeugung eines hochauflösenden Bildes anhand der Skizze

    (sketch-refinement process) -->

### StackGAN-Training

* Ergebnis von $G_i$ ist Input für $G_{i+1}$
  → Konzept entspricht cGAN
* $D_i$ bewertet Ergebnis von $G_i$
* Training der einzelnen GANs aufeinander aufbauend (von unten nach oben)

<!-- * TODO: passendes Bild -->
<!---
TODO: Also 2 Trainings in einer Epoche oder?
--->

<!-- * Frage: Welche neue Anforderung haben wir daher an die Datensätze? -->

### StackGAN-Beispielarchitektur

<!--- <img src=./graphics/Stage1_and_2_G_and_D.png> --->
* Stufe 1: $G_1$ und $D_1$ aus cGAN-Beispiel
* Stufe 2:
  * Skizze aus Stufe 1 ist Input
  * Kein Rauschvektor $z$ als Input, da Skizze bereits Rauschen enthält
  * Erzeuge hochauflösendes Bild anhand der Skizze
* *Sketch-refinement process*

---

![](graphics/Stage1_and_2_G_and_D.png){height=100%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

---

![](graphics/Stage1_and_2_G_and_D.png){height=100%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

Warum erhält der Stage-2-Generator wieder das Embedding aus dem ersten Schritt?

<!-- Warum erhält der Stage-2-Generator keinen Rausch-Vektor? -->

### Sketch-Refinement-Beispielergebnisse

<!--- <img src=./graphics/Stage2_G_results.png> --->
![](graphics/Stage2_G_results.png){width=65%}
<small> Quelle: [StackGAN: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks (2016)](https://arxiv.org/abs/1612.03242) </small>

<!--- TODO (?)
# Progressive Growing of GANs
--->
