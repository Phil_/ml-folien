# Anhang

## Deep Convolutional GAN

![](./graphics/dcgan_wb.png)

<small>Quelle: [https://www.researchgate.net/figure/Deep-convolutional-generative-adversarial-networks-DCGAN-for-generative-model-of-BF-NSP_fig3_331282441](https://www.researchgate.net/figure/Deep-convolutional-generative-adversarial-networks-DCGAN-for-generative-model-of-BF-NSP_fig3_331282441)</small>

## Mathematische Definitionen

-  $D_{\theta^{(D)}} \equiv D: \mathbb{R}^m \rightarrow [0, 1]$

    $\Rightarrow$  Wahrscheinlichkeit, dass Datum aus $\mathbb{P}_X$

-  $G_{\theta^{(G)}} \equiv G: \mathbb{Z} \rightarrow \mathbb{R}^m,\ Z: (\Omega, \mathcal{A}, \mathbb{P}) \rightarrow (\mathbb{Z}, \mathcal{B})$

    $\Rightarrow$ Sample-Generierung

- **Ziel**: $\mathbb{P}_X$ durch $\mathbb{P}_{\theta^{(G)}}$ approximieren


## Eigenschaften der Minimax-Formel

- $V(\theta^{(D)}, \theta^{(G)}) = \underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x)) + \underset{z}{\mathbb{E}} \text{ ln} (1 - D(G(z))$

1) $G$ fest $\rightarrow$ Maximum für $D^*(x) = \frac{p_X(x)}{p_X(x) + p_{\theta^{(G)}}(x)}$

2) $\theta^{(G)*} = \underset{\theta^{(G)}}{\text{arg min}}\ V(\theta^{(D)*}, \theta^{(G)})$

3) Für Minimum $\theta^{(G)*}$ gilt: $\mathbb{P}_X = \mathbb{P}_{\theta^{(G)}}$ und 

    $V(\theta^{(D)*}, \theta^{(G)*}) = -ln(4)$ 


## Trainingsprozess (Minimax)

- Training von $D$ mittels Supervised Learning

    $\Rightarrow$ **2 Minibatches** aus $X$- und $Z$-Daten

    $\Rightarrow$ Kostenfunktion $J^{(D)}$ **minimieren**: 

    \begin{align}
        J^{(D)}(\theta^{(D)}, \theta^{(G)}) = &-\frac{1}{2} \underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x))\\

                                              &- \frac{1}{2} \underset{z}{\mathbb{E}} \text{ ln}(1 - D(G(z)))
    \end{align}


## Trainingsprozess (Minimax)

- Training von $G$

    $\Rightarrow$ **1 Minibatch** aus $Z$-Daten

    $\Rightarrow$ Kostenfunktion $J^{(G)}$ **minimieren**: 

    $J^{(G)}(\theta^{(D)}, \theta^{(G)}) = - J^{(D)}(\theta^{(D)}, \theta^{(G)})$ 

- **Vielfalt an Kostenfunktionen** für $G$
