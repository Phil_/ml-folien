# Funktionsweise von GANs

## Spieltheorie bei GANs

Diskriminator $D$ (Police) | Generator $G$ (Counterfeiter)
:-------------------------:|:-------------------------:
$\Rightarrow$ Unterscheide echte Daten von generierten | $\Rightarrow$ Überliste $D$ mit Fälschungen
$\Rightarrow$ Binäre Klassifikation | $\Rightarrow$ Bildgenerierung
 | 
- Training von **2 neuronalen Netzwerken**


## Beispiel eines MNIST-GAN

![](./graphics/mnist_gan_wb.png)

<small>Quelle: [https://medium.com/intel-student-ambassadors/mnist-gan-detailed-step-by-step-explanation-implementation-in-code-ecc93b22dc60](https://medium.com/intel-student-ambassadors/mnist-gan-detailed-step-by-step-explanation-implementation-in-code-ecc93b22dc60)</small>

## Mathematische Definitionen

- Zufälliges echtes Datum: $X \sim p_{X}$ 

- Generiertes Datum: $G_\theta(Z) \sim p_{\theta^{(G)}}$ 

    $\Rightarrow$ Zufälliges Rauschen $Z$, Parameter $\theta^{(G)}$

- $\mathbb{P}_X :=$ Verteilung der echten Daten $X$

- $\mathbb{P}_{\theta^{(G)}} :=$ Verteilung der generierten Daten $G_\theta(Z)$


## Mathematische Definitionen

- $D(x) =$ Wahrscheinlichkeit, dass Datum aus $\mathbb{P}_X$

- $G(z) =$ Generiertes Sample

- **Ziel**: $\mathbb{P}_X = \mathbb{P}_{\theta^{(G)}}$


## Minimax-Spiel

- **Value-Funktion** 

    $V(\theta^{(D)}, \theta^{(G)}) = \underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x)) + \underset{z}{\mathbb{E}} \text{ ln} (1 - D(G(z)))$

    $\Rightarrow$ Spezifizierung des **ganzen Spiels**

    $\Rightarrow$ Optimaler Generator: 

    $\theta^{(G)*} = \text{arg } \underset{\theta^{(G)}}{\text{min}} \underset{\theta^{(D)}}{\text{ max  }} V(\theta^{(D)}, \theta^{(G)})$

    $\Rightarrow$ Finde **Nash Equilibrium** ($\mathbb{P}_X = \mathbb{P}_{\theta^{(G)}}$)


## Exkurs: Nash Equilibrium {data-transition="slide-in none-out"}

- Finde **beste Strategie** unter Berücksichtigung der **besten Strategie des Gegners**

- Bsp.: 2 Firmen $A$ und $B$ mit einer Handelsstrategie

- $W$: Werbung, $\overline{W}$: keine Werbung

 Gewinn von A / B |  $W_B$ | $\overline{W}_B$
--- | --- | ---
$W_A$ | 10 / 10 | 20 / 0 
$\overline{W}_A$| 0 / 20 | 0 / 0

*Wo befindet sich das Nash Equilibirum?*

## Exkurs: Nash Equilibrium {data-transition="none-in slide-out"}

- Finde **beste Strategie** unter Berücksichtigung der **besten Strategie des Gegners**

- Bsp.: 2 Firmen $A$ und $B$ mit einer Handelsstrategie

- $W$: Werbung, $\overline{W}$: keine Werbung

 Gewinn von A / B |  $W_B$ | $\overline{W}_B$
--- | --- | ---
$W_A$ | <span style="color:blue">10 / 10</span> | 20 / 0 
$\overline{W}_A$| 0 / 20 | 0 / 0

*Niemand wird benachteiligt als auch bevorzugt*

## Trainingsalgorithmus (Minimax)

- $V(\theta^{(D)}, \theta^{(G)}) = \underset{x \sim p_{X}}{\mathbb{E}} \text{ ln}(D(x)) + \underset{z}{\mathbb{E}} \text{ ln} (1 - D(G(z))$

![](./graphics/gan_alg.png)


## Klassische Kostenfunktionen von $G$

- **Stabilitätsprobleme** bei Minimax

- **Vanishing Gradient** bei $G$, wenn $D$ gut trainiert

$\Rightarrow$ Wahl anderer Kostenfunktionen für $G$

## Klassische Kostenfunktionen von $G$

0) Maximum-Likelihood

    $$\theta^{(G)*} = \underset{\theta^{(G)}}{\text{arg max}} \overset{m}{\underset{i=1}{\prod}} p_{\theta^{(G)}}(x^{(i)})$$
    
1) Kullback-Leibler-Divergenz (äquivalent zu ML)

    $$KL(\mathbb{P}_X || \mathbb{P}_{\theta^{(G)}}) = \int_{-\infty}^{\infty} p_{X}(x) \cdot \text{ln} \left( \frac{p_{X}(x)}{p_{\theta^{(G)}}(x)} \right)dx$$

    $\Rightarrow$ **Asymmetrisch**, **unbeschränkt** u. zu **minimieren**

## Klassische Kostenfunktionen von $G$

![](./graphics/KL_div_wb.png)

<small>Quelle: [https://arxiv.org/abs/1701.00160](https://arxiv.org/abs/1701.00160)</small>

## Klassische Kostenfunktionen von $G$

2) Jensen-Shanon-Divergenz (JS-Divergenz)

    \begin{align}
    JS(\mathbb{P}_X||\mathbb{P}_{\theta^{(G)}}) =&\ KL \left( \mathbb{P}_X || \frac{\mathbb{P}_X + \mathbb{P}_{\theta^{(G)}}}{2} \right)\\

                                    +&\ KL \left( \mathbb{P}_{\theta^{(G)}} || \frac{\mathbb{P}_X + \mathbb{P}_{\theta^{(G)}}}{2} \right)
    \end{align}

    $\Rightarrow$ **Symmetrisch**, **endlich** und zu **minimieren**

    $\Rightarrow$ **Erfolg von GANs**
