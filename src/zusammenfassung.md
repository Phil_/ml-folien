# Zusammenfassung

---

- Generative Modelle
- Interne Funktionsweise von GANs
- Probleme beim Training + Lösungen
- WGAN
- Konditionale Generierung
- StackGAN

# Ausblick

## CycleGAN

<iframe data-src="https://junyanz.github.io/CycleGAN/" style="height:600px;width:900px;"> 
</iframe>

## Speech2Face


![](./graphics/speech2face.png){ height=550 }

<small>[https://arxiv.org/pdf/1905.09773.pdf](https://arxiv.org/pdf/1905.09773.pdf)</small>

## cosmoGAN 

![](./graphics/cosmogan.png)

<aside class="notes">
- DCGAN (deep convolutional gan)
</aside>

## Alias-Free GAN

<iframe data-src="https://nvlabs.github.io/alias-free-gan/" style="height:500px;width:700px;">
</iframe>

---

![](https://nvlabs-fi-cdn.nvidia.com/_web/alias-free-gan/videos/video_2_metfaces_interpolations.mp4)

---

![](https://nvlabs-fi-cdn.nvidia.com/_web/alias-free-gan/videos/slice-vid.mp4)

# Übung

Datensatz: MNIST

Aufgaben: 

- Vanilla GAN Implementation
- Theorie -> Nash-Equilibrium
- GAN mit C -> Conditional GAN
