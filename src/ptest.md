</style> 

# Funktionsweise von GANs

## Einstieg in die Spieltheorie

- Spieler-Gegenspieler-Prinzip

Police (Polizei)           |  Counterfeiter (Fälscher)
:-------------------------:|:-------------------------:
![](https://i.imgur.com/qnizgQU.png){width=15%} | ![](https://i.imgur.com/GyMrZ3M.png){width=15%}
$\Rightarrow$ Falschgeld aufdecken | $\Rightarrow$ Möglichst echtes Geld machen
$\Rightarrow$ Kenntnis über echtes Geld und Falschgeld nötig | $\Rightarrow$ Kenntnis über Erfahrungsgrad der Polizei

- Qualitätsbesserung durch Konkurrenzkampf
