from matplotlib import pyplot as plt
from matplotlib import rc
import numpy as np

def plot_low_dim_manifold():
    fig = plt.figure()
    ax = plt.subplot(111)
    true_mani = ((0, 0), (0,1))
    model_mani = lambda theta: ((theta, theta), (0, 1))
    plt.plot(*true_mani, label="wahre Vert.")
    plt.plot(*model_mani(-0.5), label=r"$\theta_1$")
    plt.plot(*model_mani(0.5), label=r"$\theta_2$")
    plt.title(r"Lernen einer Verteilung im $\mathbb{R}^2$")
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=3, framealpha=0)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height*0.8])
    plt.arrow(x = 0.5, y = 0.5, dx = -0.175, dy = 0, head_width=0.05)
    plt.arrow(x = -0.5, y = 0.5, dx = 0.175, dy = 0, head_width=0.05)
    plt.tight_layout(pad=3)
    # plt.show()
    fig.savefig("low_dim_manifold.pdf", transparent=True)
    

def plot_js_vs_em():
    fig = plt.figure()
    ax = plt.subplot(111)
    js = np.vectorize(lambda x: np.log(2) if x != 0 else 0)
    em = np.abs
    x = np.linspace(-1,1,21)
    ax.scatter(x,js(x), c="blue", s=25, marker="o", label = r"$JS(\mathbb{P}_{\theta},\mathbb{P}_{X})$")
    
    ax.scatter(x,em(x), c="red", marker="x", s=25, label = r"$W(\mathbb{P}_{\theta},\mathbb{P}_{X})$")
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=2, framealpha=0)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height*0.8])
    plt.tight_layout(pad=3)
    plt.title("Wasserstein-1 vs Jensen-Shannon", y=1.05, pad=1)
    #plt.show()
    fig.savefig("em_vs_js.png", transparent=True)

if __name__ == "__main__":
    rc("font", **{"size": 20})
    plot_js_vs_em()