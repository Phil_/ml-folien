# Allgemeines

---

- Folien unter [phil_.gitlab.io/ml-folien/](https://phil_.gitlab.io/ml-folien/)
- Fragen jederzeit willkommen :)


# Inhalt

- Einleitung bzw. Motivation
- Funktionsweise von GANs
- Schwierigkeiten beim Training + Lösungen
- WGAN
- Conditional Generation
- StackGAN
- Zusammenfassung + Übung


# Einleitung

<!---
- Funktionsweise von GANs [Philipp]
    - Einstieg Spieltheorie-Minimax -> Kleine Wahrheitstabellen
    - Spieler-Gegenspieler-Prinzip (Police vs. Counterfeiter)
    - Diskriminator $D$ (binäre Klassifikation; Polizei, die Falschgeld aufdecken will)
    - $D: \mathbb{R}^m \rightarrow [0, 1]$ (Wahrscheinlichkeit, dass Datum aus $\mathbb{P}_X$)
    - $D$ bewertet Unterschied zwischen echten und falschen Daten
    
- Was ist Generative Modellierung? [Philipp]
    - Motivation: Warum generative Modellierung?
    - Wie funktionieren generative Modelle? (Maximum-Likelihood)
    - Vergleich zu anderen bekannten Modellen
        - MSQ-Modell vs Multimodal-Output-Modell
        - Schwächen der bisherige Modelle z.B. unscharfe Bilder
    - Preview auf Anwendung
    - Nachteile
    - Eigenschaften von GANs
-->

## Generative Modellierung

### Motivation: Klassifikator

\[ \mathbb{P}(Y | X) \]

<aside class="notes">
- Herkömmlicher Klassifikator lernt "Aussehen" von Daten 
- Generieren von Inputs, welche Output `True` erzeugen würden?
- Generative Modelle
</aside>


---

![](https://cdn-images-1.medium.com/max/1200/1*oB3S5yHHhvougJkPXuc8og.gif){ width=90% }

<small>Quelle: [https://becominghuman.ai/building-an-image-classifier-using-deep-learning-in-python-totally-from-a-beginners-perspective-be8dbaf22dd8](https://becominghuman.ai/building-an-image-classifier-using-deep-learning-in-python-totally-from-a-beginners-perspective-be8dbaf22dd8)</small>

---

#### Generative Modellierung:

\[ \mathbb{P}(Y | X) \rightarrow \mathbb{P}(X, Y) \]

### Was ist generative Modellierung?

\[ \mathbb{P}(X,Y) \]

- Input: Verteilung $X$ ("observable")
    - Features
- Output: Verteilung $Y$ ("target")
    - Label / Bild / ...
- gesucht: Wahrscheinlichkeitsverteilung $\mathbb{P}(X, Y)$

=> Approximation der Verteilung $\mathbb{P}$

<aside class="notes">
Zumeist Hochdimensionale Verteilungen  
</aside>

---

![](./graphics/wine-bottles.png){ width=66% }

<small>Quelle: [https://www.researchgate.net/figure/An-illustration-of-the-difference-between-the-generative-and-discriminative-models-in_fig9_319093376](https://www.researchgate.net/figure/An-illustration-of-the-difference-between-the-generative-and-discriminative-models-in_fig9_319093376)</small>

--- 

Nach dem Training kann das generative Modell die Funktion

\[ f: X \rightarrow Y \]

abbilden.

<aside class="notes">
- explizit vs implizites Abbilden
</aside>


### Beispiele Generativer Modelle

- Bayesische Netzwerke
- Boltzmann Maschinen
- VAEs
- GANs
- Flussbasierte generative Modelle
- ...


## GANs

### Aufbau

###

Police (Polizei)           |  Counterfeiter (Fälscher)
:-------------------------:|:-------------------------:
![](./graphics/police.png){width=25\%} | ![](./graphics/counterfeiter.png){width=25\%}
$\Rightarrow$ Falschgeld aufdecken | $\Rightarrow$ Möglichst echtes Geld machen
$\Rightarrow$ Kenntnis über echtes Geld und Falschgeld nötig | $\Rightarrow$ Kenntnis über Erfahrungsgrad der Polizei


<aside class="notes">
- Spieler-Gegenspieler-Prinzip
- Qualitätsbesserung durch Konkurrenzkampf

- 2 Spieler Prinzip  
- Fälscher / Polizei
</aside>


---

![](./graphics/gan-concept.png)
<small>Quelle: [https://www.slideshare.net/xavigiro/deep-learning-for-computer-vision-generative-models-and-adversarial-training-upc-2016](https://www.slideshare.net/xavigiro/deep-learning-for-computer-vision-generative-models-and-adversarial-training-upc-2016)</small>

### Funktionsweise

1. Training (Loop)
    1. Generator generiert "zufällige" Daten
    1. Diskriminator klassifiziert
    1. Diskriminator wird mit Trainingsset trainiert
    1. Trainingsschritt für Generator
1. Generator alleinstehend nutzen zum Generieren von neuen Daten


### Motivation

- Modellierung hochdimensionaler Datensätze
- Einbinden in Reinforcement Learning
    - Simulationen
- Auffüllen von Datensätzen / Vorhersagen von fehlenden Daten
    - Semi-supervised Learning
- Generieren von Katzenbildern


### Beispiele

---

<small> https://thispersondoesnotexist.com </small>

<iframe data-src="https://thispersondoesnotexist.com/" style="height:500px;width:700px;">
</iframe>
