---
title: "Vorbesprechung zur Präsentation zu GANs"
author: [Michael Gordon, Jonah Kerber, Philipp Herzog, Benedikt Scherer, Josef Azzam]
date: "2021-06-03"
subject: "Generative Adversarial Networks"
keywords: [GAN, Deeplearning, Generative Modeling]
lang: "de"
slideOptions:
  theme: white
  slideNumber: false
---

<style>
.reveal {
    font-size: 24px
}
</style>


# Generative Adversarial Networks


---

## Inhalte


---

### Was ist Generative Modellierung? [Philipp]
- Motivation: Warum generative Modellierung?
- Wie funktionieren generative Modelle? (Maximum-Likelihood)
- Vergleich zu anderen bekannten Modellen
    - MSQ-Modell vs Multimodal-Output-Modell
    - Schwächen der bisherige Modelle z.B. unscharfe Bilder
- Preview auf Anwendung
- Nachteile
- Eigenschaften von GANs

---


### Was ist ein GAN? [Josef]


---

- Funktionsweise von GANs [Josef]
    - Einstieg Spieltheorie-Minimax -> Kleine Wahrheitstabellen
    - Spieler-Gegenspieler-Prinzip (Police vs. Counterfeiter)
    - Diskriminator $D$ (binäre Klassifikation; Polizei, die Falschgeld aufdecken will)
    - $D: \mathbb{R}^m \rightarrow [0, 1]$ (Wahrscheinlichkeit, dass Datum aus $\mathbb{P}_X$)
    - $D$ bewertet Unterschied zwischen echten und falschen Daten
    - Generator $G$ (Diskriminator mit Fälschungen austricksen; Counterfeiter/Fälscher)
    - $G: \mathbb{Z} \rightarrow \mathbb{R}^m,\ Z: (\Omega, \mathcal{A}, \mathbb{P}) \rightarrow (\mathbb{Z}, \mathcal{B})$ $\Longrightarrow$ Mit Rauschen $Z$ und $G$ neue Daten $G(Z)$ erzeugen
    - $G$ erlernt Verteilung der echten Daten
    $\Rightarrow$ Spiel und kein Optimierungsproblem
    $\Rightarrow$ Finde Nash Equilibrium statt lokales Minimum
    - Minmax: Spezifizierung des kompletten Spiels

---

- klassische Kostenfunktionen (ggf erwähnen, dass es auch andere gibt) [Josef]
    - Maximum-Likelihood
    - Kullback-Leibler-Divergenz
    - Jensen-Shanon-Divergenz

- GAN Trainingsalgorithmus [Jonah/Josef??]

---

### Was sind die Hürden bei GANs? [Jonah]

---

- Schwierigkeiten / Probleme [Jonah]
    - mode 
        - Kullback Leibler macht wegen fehlender Symmetrieeigenschaften Probleme
    - Non-convergence
    - Instability   

---

- Tipps und Tricks [Jonah]
    - Trainieren mit Labels
    - one-sided label smoothing
    - Virtual batch normalization
    - Macht Balance zwischen $G$ und $D$ Sinn? 
    - [GAN Hacks](https://github.com/soumith/ganhacks)

---

### Wo benutzt man GANs /  The GAN Zoo? [Michael und Benedikt]
<!--- 
TODO: Taxanomie und Anwendung mergen in thematisch zusammenhängende Bereiche: 
- (Problem, Theorie, Beispiel)
Tabelle mit Kategorien
--->

---

- Taxanomie der GANs [Michael & Benedikt]
    - Conditional Generation [Benedikt]
    - Multiple Generator and Multiple Discrimiators (SGAN) [Benedikt]
    - Joint Architecture [Benedikt]
    - Improved Discrimiators [Benedikt]
    - Memory Networks [Michael]
    - Latent space engineering [Michael]
    - Neue Loss-Funktionen (z.B. WGAN) [Michael]
    - Regularisierte GANs [Michael]


---

- Anwendungen [Michael & Benedikt]:
    - Generierung neuer Bilder
        - [StyleGAN2](https://thispersondoesnotexist.com/)  
    - text-to-image
    - Aufbereitung von Bildqualität bzw. Änderung des Bildtyps
        - Hier gäbe es viele verschiedene spezifische Beispiele (Foto zu Kunststil ist schön)
    - Kombination von text-to-image und Bildaufbereitung/-änderung mittels StackGAN
        - Erzeugung von fotorealistischen Bildern aus Text
    - DeepFakes

---

## Zusammenfassung und Ausblick [Philipp]
- Review Vorlesung
- [VAEs für Gesichter statt GANs](https://syncedreview.com/2019/06/06/going-beyond-gan-new-deepmind-vae-model-generates-high-fidelity-human-faces/)
- Kurz Übung einführen    


---

## Übung
- MNIST Vanilla GAN
- Conditional (W)GAN
- StyleGAN auf Google Colab?
- SGAN/StackGAN Vorimplementierung anwenden lassen
- Goodfellow Tutorial Exercise 2
- Zusatzaufgabe: Goodfellow Tutorial Excercise 1
- Zusatzaufgabe: Eigenschaften von Minmax-Formel
- Inkrementell mit Zwischenstand gegeben teilweise (wie Bialonski Klausur)
- Vanilla GAN (MNIST)
- Student muss sinnvolle Architektur aus multiple choice auswählen


---

## Sicht des Studierenden
- bloß keine Maßtheorie!
- coole Anwendungsbeispiele
- Quellenverzeichnis, Fußnoten
- Einfache bzw. verständliche Übung
- Kein großer Zeitdruck
