{
  description = "Default Python flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    mach-nix.url = "github:DavHau/mach-nix";
    nur.url = "github:nix-community/NUR";
  };

  outputs = { self, nixpkgs, flake-utils, mach-nix, nur, ... }:
  flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        nur.overlay
      ];
    };

    mach-nix-util = import mach-nix {
      inherit pkgs;
      python = "python39";
    };

    customPython = mach-nix-util.mkPython {
      requirements = (builtins.readFile ./requirements.txt);
    };

    python3 = customPython.python.pkgs;

    script = { ... }@args: pkgs.nur.repos.ysndr.lib.wrap ({ shell = true; } // args);

    reqs = with pkgs; [
        customPython
        pandoc
        gnumake
    ];

  in rec {
    devShell = pkgs.mkShell {
      buildInputs = reqs ++ [
        pkgs.gitlab-runner
        pkgs.tree
      ];
    };

    packages.compile = script {
      name = "generate-slides";
      paths = reqs ++ [ pkgs.tree ];
      script = ''
        OUTFILE=index.html make
      '';
    };

    apps.compile = flake-utils.lib.mkApp {
      drv = packages.compile;
      exePath = "";
    };

    defaultPackage = packages.compile;
    defaultApp = apps.compile;
  });
}
