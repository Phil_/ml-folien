# Notizen

## Motivation

### Warum überhaupt generative Modelle?

- Verständnissgewinung zu hochdimensionalen Verteilungen
- Einbau in Reinforcement Learning -> generative Modelle erzeugen mögliche Szenarien
- Semi-Supervided Learning: Datensätze, in denen viele Labels fehlen
- Multi-modale Outputs
- Erzeugung von sehr realitätsnahen Daten

## Beispiele
- Bilder
    - Frame-Interpolation (2 Minute Paper Quelle finden)
    - Single image super-resolution (SRGAN)
    - Kunsterzeugung (StyleGAN)
    - Bild-zu-Bild-Translation (Sattelite Aerial to Map)
- Ton-Spuren
- Welche Architekturen passen zu welchen Anwendungen?
- Text (z.B. GPT-3)

## Vergleich mit anderen Modellen

- kurz nennen

## Grundsätzliches

- Simultanes Training von zwei Modellen
    1) Generator Netzwerk $G$ 
        --> Erzeugung von Daten (z.B. zufälliges Bild)
    3) Diskriminator Netzwerk $D$ 
        --> Analyse der generierten Daten
        --> Training zuvor mit echten Daten
- Ziel: $G$ so trainieren, dass $D$ maximalen Fehler macht, das heißt $D$ kann generierte Daten nicht mehr von echten unterscheiden
- Schritte
    1) Definiere Problem
    2) Definiere GAN-Architektur (zwei neuronale Netze)
    3) Trainiere $D$-Modell (Unterscheidung zwischen Real und Fake)
    4) Trainiere $G$-Modell (Wettkampf/Kooperation mit D, Spieler-Gegenspieler-Prinzip)
    5) Wiederhole 3), 4) N mal
    6) Synthetisiere Daten von $G$
- Fehlerfunktion: $D$ 
    --> Kreuzentropie Fehler mit Logits

## Typen

- Deep Convolutional GAN (CNN)
![](https://cdn.discordapp.com/attachments/768376459969429505/842333505400995860/unknown.png)

- Conditional GAN 
    --> https://machinelearningmastery.com/how-to-develop-a-conditional-generative-adversarial-network-from-scratch/
- Information GAN
- Wasserstein GAN
- Attention GAN
- Coupled GAN

## Anton's Dokument

### Einleitung

- Kontext: Bilder von Gesichtern, Ton-Spuren von Musik usw.
- Ähnliche Daten neu erzeugen
- Wahrscheinlichkeitsverteilung $\mathbb{P}_X$ der echten Daten approximieren
--> Merkmale erlernen, Approximation $\mathbb{P}_\theta$
--> Kullback-Leibler-Divergenz minimieren

$$
KL(\mathbb{P}_X || \mathbb{P}_\theta) = \int f_X \cdot ln \frac{f_X}{f_\theta} d\mu \ge 0\ (= 0 \text{, wenn Verteilungen gleich})
$$

- $f_X > f_\theta$: Daten stammen eher aus $\mathbb{P}_X$
    --> $f_\theta \rightarrow 0 \Rightarrow KL \rightarrow \infty$ (starke Strafe)
- $f_X < f_\theta$: Daten stammen eher aus 1. Verteilung (wenig sinnvolle Bilder)
    --> $f_X \rightarrow 0 \Rightarrow KL \rightarrow 0$ (schwache Strafe)

### Klassisches GAN

- Spieler-Gegenspieler-Prinzip
- Generator $G$ erlernt Verteilung $\mathbb{P}_X$ der echten Daten $X \Rightarrow \mathbb{P}_G$
- Diskriminator $D$ bewertet Unterschied zwischen $\mathbb{P}_X$ und $\mathbb{P}_G$
- $G: \mathbb{Z} \rightarrow \mathbb{R}^m,\ Z: (\Omega, \mathcal{A}, \mathbb{P}) \rightarrow (\mathbb{Z}, \mathcal{B})$ $\Longrightarrow$ Mit Rauschen $Z$ und $G$ neue Daten $G(Z)$ erzeugen
- $D: \mathbb{R}^m \rightarrow [0, 1]$ (Wahrscheinlichkeit, dass Datum aus $\mathbb{P}_X$)

### Min-Max-Formel

$$ \underset{G}{min}\ \underset{D}{max}\ \underbrace{\mathbb{E}[ln(D(X))] + \mathbb{E}[ln(1-D(G(Z)))]}_{=: V(D, G)}\ \ \ \text{(Definition des Trainings)} $$

- Erster $\mathbb{E}$-Wert groß, wenn $D$ sicher auf echten Daten ($D(X) = 1$)
- Zweiter $\mathbb{E}$-Wert groß, falls $D$ $\mathbb{P}_X$ und $\mathbb{P}_G$ perfekt unterscheiden kann ($D(G(Z)) = 0$)
    --> Maximierung
- Wenn $G$ ähnliche Daten wie $\mathbb{P}_X$ erzeugen kann
    --> Abnahme der Sicherheit von $D$ ($D(X) = 0$, $D(G(Z)) = 1$)
    --> Minimierung
    
### Eigenschaften der Formel

1) $G$ fest $\rightarrow$ $D$ globales Maximum bei $D^* = \frac{f_X}{f_X + f_G}$
2) $C(G) := V(D^*, G) \Rightarrow \underset{G}{min}\ C(G)$
3) Für globales Minimum $G^*$ von $C(G)$ gilt: $\mathbb{P}_X = \mathbb{P}_{G^*}$ und $C(G^*) = -ln(4)$ 

- Jensen-Shannon-Divergenz (JS-Divergenz): 
 
$$ 
\begin{align}
V(D^*, G) &= \mathbb{E}\left[ln \frac{f_X(X)}{f_X(X) + f_G(X)}\right] + \mathbb{E}\left[ln \frac{f_G(G(Z))}{f_X(G(Z)) + f_G(G(Z))}\right]\\
&= \\
&=: C(G) 
\end{align}$$

$$ JS(\mathbb{P}_X||\mathbb{P}_G) = \frac{1}{2} (KL(\mathbb{P}_X || \frac{\mathbb{P}_X + \mathbb{P}_G}{2}) + KL(\mathbb{P}_G || \frac{\mathbb{P}_X + \mathbb{P}_G}{2}))$$


### Beweis der Eigenschaften

[...]

### Trainingsalgorithmus

[...]

### Stabilitätsprobleme

- Erfolg abhängig von Architektur von $D$ und $G$

- $G$-Training schwieriger, je besser die Qualität von $D$

- Konvergenz von $D \not\Rightarrow$ Konvergenz von $G$

- Lösung: $\textbf{Wasserstein GANs}$ (WGANs)


## Goodfellow

### 50 Seiten Tutorial

#### Kapitel 1:

#### Kapitel 2:

#### Kapitel 3:

#### Kapitel 4:

#### Kapitel 5: 


### Beweisaufgabe

$X \sim F_{data} \text{ mit Dichte } p_{data},\ G(Z) \sim F_{model} \text{ mit Dichte } p_{model}$

$$
\begin{align}
J^{(D)}(\theta^{(D)}, \theta^{(G)}) 
&= -\frac{1}{2} \mathbb{E}_{X \sim p_{data}}\ ln(D(X)) -\frac{1}{2}  \mathbb{E}_Z\ ln(1-D(G(Z)))\\ 
&= -\frac{1}{2} \int ln(D(X))\ d\mathbb{P} -\frac{1}{2} \int ln(1 - D(G(Z)))\ d\mathbb{P}\\
&\stackrel{\text{TS}}{=} -\frac{1}{2} \int ln(D(x))\ \mathbb{P}_X(dx) -\frac{1}{2} \int ln(1 - D(x))\ \mathbb{P}_{G(Z)}(dx)\\
&\stackrel{\text{RN}}{=} -\frac{1}{2} \int ln(D(x)) \cdot p_{data}(x)\ \lambda(dx) -\frac{1}{2} \int ln(1 - D(x)) \cdot p_{model}(x)\ \lambda(dx) =: J(x)
\end{align}
$$

$$
\frac{d}{d D(x)} J(x)
= -\frac{1}{2} \int \frac{1}{D(x)} \cdot p_{data}(x)\ dx +\frac{1}{2} \int \frac{1}{1 - D(x)} \cdot p_{model}(x)\ dx \stackrel{!}{=} 0
$$

$$
\begin{align}
&\Longleftrightarrow \int \frac{1}{D(x)} \cdot p_{data}(x) - \frac{1}{1 - D(x)} \cdot p_{model}(x)\ dx = 0 \\
&\Longrightarrow \frac{1}{D(x)} \cdot p_{data}(x) - \frac{1}{1 - D(x)} \cdot p_{model}(x) =0 \\
&\Longleftrightarrow D^{*}(x) = \frac{p_{data}(x)}{p_{data}(x) + p_{model}(x)}
\end{align}
$$


## Übungsidee

- Handgeschriebene Ziffern über MNIST-Daten erzeugen

## Aufgabeneinteilung
- Jonah: Trainingalgorithmus, Kapitel 5 von 50 S. vielleicht noch?? (da werden Nachteile aufgelistet)
- Josef: Beweise, Herleitung Jensen Shanon, Zusammenfassung 50 S. Tutorial, Kapitel 1-4

- Benedikt: Beispiel Recherche
- Philipp: Übung
- Michael: Special Flavours von GANs

## Meetings
- 04.06 Meeting mit Professor B.
- 02.07. Vorlesung

## HIIIIIILFE

D:

