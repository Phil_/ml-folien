# Links
Sinn dieses Repos ist es diese Datei zu enthalten und in Gitlab anzuzeigen. Hier sammeln wir Links für das Projekt. 
## Papers
1. [Genrative Adversarial Nets](https://arxiv.org/abs/1406.2661)
2. [Unsupervised Represantation Learning with Deep Convulutional Generative Adverserarial Networks](https://arxiv.org/pdf/1511.06434.pdf)
3. [Large Scale GAN Training for High Fidelity Natural Image Synthesis](https://arxiv.org/abs/1809.11096)
4. [Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network](https://arxiv.org/pdf/1609.04802.pdf)
5. [Wasserstein GAN](https://arxiv.org/abs/1701.07875?source=post_page-----aee68ed8a38c----------------------)
6. [Conditional Wasserstein Generative Adversarial Networks](https://cameronfabbri.github.io/papers/conditionalWGAN.pdf)
7. [On distinguishability criteria for estimating generative models](https://arxiv.org/pdf/1412.6515.pdf)
8. [Towards Principled Methods for Training Generative Adversarial Networks](https://arxiv.org/abs/1701.04862)
9. [NIPS 2016 Tutorial: Generative Adversarial Networks](https://arxiv.org/pdf/1701.00160.pdf?source=post_page---------------------------)

## Blogs 
1. [Fantastic GANs and where to find them](http://guimperarnau.com/blog/2017/03/Fantastic-GANs-and-where-to-find-them)
2. [Fantastic GANs and where to find them](http://guimperarnau.com/blog/2017/11/Fantastic-GANs-and-where-to-find-them-II)
3. [BigGAN: A New State of the Art in Image Synthesis](https://medium.com/syncedreview/biggan-a-new-state-of-the-art-in-image-synthesis-cf2ec5694024)
4. [Imaginary worlds dreamed by BigGAN](https://aiweirdness.com/post/178619746932/imaginary-worlds-dreamed-by-biggan)
5. [Eine kurze Einführung in Generative Adversarial Networks](https://blog.codecentric.de/2018/11/eine-kurze-einfuehrung-in-generative-adversarial-networks/ "Eine kurze Einführung in Generative Adversarial Networks")
6. [Best Resources for Getting Started With GANs](https://machinelearningmastery.com/resources-for-getting-started-with-generative-adversarial-networks/)
7. [Introducing “Lucid Sonic Dreams”: Sync GAN Art to Music with a Few Lines of Python Code!](https://towardsdatascience.com/introducing-lucid-sonic-dreams-sync-gan-art-to-music-with-a-few-lines-of-python-code-b04f88722de1)
[Dazugehöriges Colab Notebook](https://colab.research.google.com/drive/1Y5i50xSFIuN3V4Md8TB30_GOAtts7RQD?usp=sharing)
## Scibo
1. [Scibo, in dem PDFs, Bilder, Videos und andere Medien Leben](https://fh-aachen.sciebo.de/s/PN4jJwRY08rc07u)

## Git-Repos
1. [The GAN Zoo](https://github.com/hindupuravinash/the-gan-zoo)
2. [How to Train a GAN? Tips and tricks to make GANs work](https://github.com/soumith/ganhacks)
3. [Lucid Sonic Dreams](https://github.com/mikaelalafriz/lucid-sonic-dreams)
4. [really awesome gans](https://github.com/nightrome/really-awesome-gan)

## Youtube
1. [The Math Behind Generative Adversarial Networks Clearly Explained!](https://www.youtube.com/watch?v=Gib_kiXgnvA&t=136s)
2. [GAN Generates English & Bengali Alphabets | How to Train a GAN?](https://www.youtube.com/watch?v=RzcXri2eJrI&t=905s)

## Stoffsammlung

1. [Shared Markdown Datei](https://hackmd.io/UZlHWXd7QqiQ114UrscHSA?bothf)

## Übungsnotebook

1. [Jupyter Notebook von der Übung](https://deepnote.com/project/GAN-Ubung-UiUj632OSuq_ST0C02xBFQ/%2Fgan_uebung.ipynb/#00002-234eba39-a937-4b5d-8e18-0bc7cdb734e4)
2. [Vanilla GAN](https://wiki.pathmind.com/generative-adversarial-network-gan)
