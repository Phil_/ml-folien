---  
title: "Wissenswertes zu GANs"  
author: [Michael Gordon]  
date: "2021-05-19"  
subject: "Generative Adversarial Networks"  
keywords: [GAN, Deeplearning, Generative Modeling]  
lang: "de"  
institute: Forschungszentrum Jülich, IEK-5  
...  

# Einleitung

## Einstieg in die Spieltheorie

Police (Polizei)           |  Counterfeiter (Fälscher)
:-------------------------:|:-------------------------:
![](./graphics/police.png){width=25%} | ![](./graphics/counterfeiter.png){width=25%}
$\Rightarrow$ Falschgeld aufdecken | $\Rightarrow$ Möglichst echtes Geld machen
$\Rightarrow$ Kenntnis über echtes Geld und Falschgeld nötig | $\Rightarrow$ Kenntnis über Erfahrungsgrad der Polizei

- Spieler-Gegenspieler-Prinzip

- Qualitätsbesserung durch Konkurrenzkampf


