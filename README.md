# Notizen zum Thema GANs

Dieses Repo dient dazu meine Recherche zum Thema GANs in Form einer Präsentation zu bündeln.

In diese Präsentation gingen folgende Quellen ein:

1. NIPS 2016 Tutorial: Generative Adversarial Networks von Ian Goodfellow.


## Dependencies

Um diese Präsentation anzuzeigen wird [pandoc](https://pandoc.org/) benötigt sowie, 
die Pandoc Filter:

1. [pandoc-xnos](https://github.com/tomduck/pandoc-xnos)

Um die Präsensation als PDF zu erzeugen wird [TexLive](https://tug.org/texlive/) sowie
das beigefügte Corporate-Design der FH-Aaachen für Beamer. 

Das Beamer Theme muss OS-Abhängig
installiert werden. Der Installationspfad kann mit `kpsewhich -var-value TEXMFLOCAL` ermittelt werden.

Zur Installation muss das Archiv in diesem Pfad entpackt werden.

Ebenfalls wird [GNU Make](https://www.gnu.org/software/make/) benutzt

## Installation

Sind alle Dependencies vorhanden, reicht es aus das repo zu clonen.

## Generierung der Präsentationen

Zur Generierung der Präsentationen reicht es `make` aufzurufen. 

Zum Löschen der generierten Daten wird der Befehl  `make clean` aufgerufen.