# Was ist generative Modelierung?

## Theoretischer Blickwinkel
- Annahme: Daten sind Stichproben einer Zufallsgröße
- Ziel: Finden der Verteilung Anhand der Daten
- Anwendung: Erzeugung neuer Daten


# Praktischer Blickwinkel

## Bilder
- Superresolution
\center
![Superresolution](graphics/superresolution.pdf){width=50%}


## Tonspuren
- Musicvisualisierung

## Unterstützungen von anderen Machienlearning-Bereichen
- Verbesserung von Reinforcement Learning
- Semi-Supervised Learning

# Let's test some features

## A plain math environment

<!--- 
{#eq:label} zum labeln von Gleichungen
@eq:label zum referenzieren des Gleichungen
--->
$$ \Phi(x) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^x e^{\frac{-x^2}{2}} $$ {#eq:Standardnormalverteilung}
In Gleichung @eq:Standardnormalverteilung sieht man die Verteilungsfunktion der Standardnormalverteilung

## Different Math
- Equation 1 $e=mc^2$


## Let's get fancy and use align!

### Solution to Goodfellow's first problem
\fontsize{8pt}{8pt}\selectfont\begin{align*}
J^{(D)}(\theta^{(D)}, \theta^{(G)})
&= -\frac{1}{2} \mathbb{E}_{x \sim p_{data}}\ ln(D(X)) -\frac{1}{2}  \mathbb{E}_z\ ln(1-D(G(Z)))\\ 
&= -\frac{1}{2} \int ln(D(X))\ d\mathbb{P} -\frac{1}{2} \int ln(1 - D(G(Z)))\ d\mathbb{P}\\
&\stackrel{\text{TS}}{=} -\frac{1}{2} \int ln(D(x))\ \mathbb{P}_X(dx) -\frac{1}{2} \int ln(1 - D(x))\ \mathbb{P}_{G(Z)}(dx)\\
&\stackrel{\text{RN}}{=} -\frac{1}{2} \int ln(D(x)) \cdot p_{data}(x)\ \lambda(dx) \\&\quad\thinspace -\frac{1}{2} \int ln(1 - D(x)) \cdot p_{model}(x)\ \lambda(dx) =: J(x)
\end{align*}

## Let's get fancy and use align!

### Solution part two
$$
\frac{d}{d D(x)} J(x)
= -\frac{1}{2} \int \frac{1}{D(x)} \cdot p_{data}(x)\ dx +\frac{1}{2} \int \frac{1}{1 - D(x)} \cdot p_{model}(x)\ dx \stackrel{!}{=} 0
$$


\begin{align}
&\Longleftrightarrow \int \frac{1}{D(x)} \cdot p_{data}(x) - \frac{1}{1 - D(x)} \cdot p_{model}(x)\ dx = 0 \\
&\Longrightarrow \frac{1}{D(x)} \cdot p_{data}(x) - \frac{1}{1 - D(x)} \cdot p_{model}(x) =0 \\
&\Longleftrightarrow D^{*}(x) = \frac{p_{data}(x)}{p_{data}(x) + p_{model}(x)}
\end{align}


## Some more refs
- Cost of the Discriminator $J^{(D)}(\theta^{(D)}, \theta^{(G)})$
- Cost of the Generator $J^{(G)}(\theta^{(D)}, \theta^{(G)})$
- Simultanious Optimization!

## Hyperlinktest
- Does this work? @eq:Standardnormalverteilung

## Images
\center
You are a very good scientist!
\center
![Scientific Puppy](graphics/scientist.jpg){width=50%}

## Table
  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax.
