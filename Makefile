SOURCE := src/metadata.yaml src/philipp.md src/josef.md src/Jonah.md src/michael.md src/benedikt.md src/zusammenfassung.md src/appendix.md

OUTFILE ?= out.html

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    removefile := del
else
    removefile := rm  # assume all other os know rm
endif

# TODO figure out how to do proper revealjs style files
#STYLE := revealjs_style.css
#THEMES: https://revealjs.com/themes/

.PHONY: all
all: html

#.PHONY: pdf
#pdf: # --citeproc removed
#	pandoc --filter pandoc-xnos -V --#from="markdown+tex_math_dollars+tex_math_single_backslash+latex_macros+simple_tables+table_captions+multiline_tables+grid_tables+pipe_tables" #--to beamer  --listings  --slide-level=2 -o out.pdf $(SOURCE)

.PHONY: html
html: # --citeproc removed
	pandoc --filter=pandoc-xnos -V theme=serif -t revealjs --mathjax --from="markdown+tex_math_dollars+tex_math_single_backslash+latex_macros+raw_tex" --slide-level=3 -s -o $(OUTFILE) $(SOURCE)

.PHONY : clean
clean :
	@echo --- Deleting generated files out.html, out.pdf ---
	$(removefile) out.html
